import random

accuracy=100000

tel_number = int(input("Введите номер телефона: "))
while len(str(tel_number)) != 11:
    tel_number = int(input("Введите номер телефона: "))

v = 0
for k in range(accuracy):
    n = [i for i in range(10)]
    for i in range(10):
        active_n = n.pop(int(random.uniform(0, len(n)-1)))
        if i <= 2 and (str(tel_number)[:-1] + str(active_n)) == str(tel_number):
            v += 1
print("Вероятность =", (v / accuracy))