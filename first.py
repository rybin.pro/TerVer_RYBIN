import random
import matplotlib
from matplotlib import pyplot as plt

# py_random=[int(random.uniform(-1,2)) for i in range(50)]
# really_random=[int(input("real: ")) for i in range(50)]
# human_random=[int(input("human: ")) for i in range(50)]

py_random = [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
             0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0]
really_random = [1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0,
                 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0]
human_random = [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0,
                0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1]


def collect_data_for_graph(arr):
    graph = {}
    count = 1
    for i in range(0, len(arr)-1):
        if arr[i] == arr[i+1]:
            count += 1
        else:
            if count not in graph:
                graph[count] = 0
            graph[count] += 1
            count = 1
    return graph
    # print(graph)


py_random_graph=collect_data_for_graph(py_random)
really_random_graph=collect_data_for_graph(really_random)
human_random_graph=collect_data_for_graph(human_random)

print(py_random_graph)
print(py_random, "\n", really_random, "\n", human_random)
#
def generate_subgraph(dict, pos):
    plt.subplot(pos)
    plt.plot(range(len(dict)), list(dict.values()))

plt.figure()

plt.subplot(311)
plt.plot(range(len(py_random_graph)), list(py_random_graph.values()))

plt.subplot(312)
plt.plot(range(len(really_random_graph)), list(really_random_graph.values()))


plt.subplot(313)
plt.plot(range(len(human_random_graph)), list(human_random_graph.values()))
# generate_subgraph(py_random_graph, 311)
plt.show()